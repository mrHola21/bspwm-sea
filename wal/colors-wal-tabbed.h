static const char* selbgcolor   = "#0c0b12";
static const char* selfgcolor   = "#bdc5cf";
static const char* normbgcolor  = "#45566B";
static const char* normfgcolor  = "#bdc5cf";
static const char* urgbgcolor   = "#464D60";
static const char* urgfgcolor   = "#bdc5cf";
