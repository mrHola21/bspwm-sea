const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0c0b12", /* black   */
  [1] = "#464D60", /* red     */
  [2] = "#45566B", /* green   */
  [3] = "#4C6678", /* yellow  */
  [4] = "#547389", /* blue    */
  [5] = "#67798E", /* magenta */
  [6] = "#708A9E", /* cyan    */
  [7] = "#bdc5cf", /* white   */

  /* 8 bright colors */
  [8]  = "#848990",  /* black   */
  [9]  = "#464D60",  /* red     */
  [10] = "#45566B", /* green   */
  [11] = "#4C6678", /* yellow  */
  [12] = "#547389", /* blue    */
  [13] = "#67798E", /* magenta */
  [14] = "#708A9E", /* cyan    */
  [15] = "#bdc5cf", /* white   */

  /* special colors */
  [256] = "#0c0b12", /* background */
  [257] = "#bdc5cf", /* foreground */
  [258] = "#bdc5cf",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
