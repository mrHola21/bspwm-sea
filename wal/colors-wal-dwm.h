static const char norm_fg[] = "#bdc5cf";
static const char norm_bg[] = "#0c0b12";
static const char norm_border[] = "#848990";

static const char sel_fg[] = "#bdc5cf";
static const char sel_bg[] = "#45566B";
static const char sel_border[] = "#bdc5cf";

static const char urg_fg[] = "#bdc5cf";
static const char urg_bg[] = "#464D60";
static const char urg_border[] = "#464D60";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
