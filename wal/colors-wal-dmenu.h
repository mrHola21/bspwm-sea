static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bdc5cf", "#0c0b12" },
	[SchemeSel] = { "#bdc5cf", "#464D60" },
	[SchemeOut] = { "#bdc5cf", "#708A9E" },
};
