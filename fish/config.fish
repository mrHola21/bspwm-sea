set fish_greeting

alias ls='exa -a --color=always --group-directories-first --icons'
alias lsa='exa -al --color=always --group-directories-first --icons'
alias tree='exa -aT --color=always --group-directories-first --icons'

alias install='sudo pacman -S'
alias update='sudo pacman -Syy'
alias upgrade='sudo pacman -Syyuu'

set -g EDITOR nvim

if status is-interactive
	echo "Hello $USER"
	echo (date) | lolcat
    # Commands to run in interactive sessions can go here
end
